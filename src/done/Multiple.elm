-- Read more about this program in the official Elm guide:
-- https://guide.elm-lang.org/architecture/effects/http.html


module Multiple exposing (..)

import IndexedList exposing (IndexedList)
import Widget exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { contents :
        IndexedList Widget.Model
    , newTopic : String
    }


init : ( Model, Cmd Msg )
init =
    ( Model IndexedList.empty ""
    , Cmd.none
    )



-- UPDATE


type Msg
    = EntryMsg Int Widget.Msg
    | UpdateNewTopic String
    | AddNewTopic
    | RefreshAll
    | AddNewCounter


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UpdateNewTopic s ->
            ( { model | newTopic = s }, Cmd.none )

        AddNewTopic ->
            let
                ( newModel, newCmd ) =
                    Widget.initGiphy model.newTopic

                ( newIdx, newContents ) =
                    IndexedList.add (newModel) model.contents
            in
                { model | contents = newContents }
                    ! [ Cmd.map (EntryMsg newIdx) newCmd ]

        EntryMsg idx widgetMessage ->
            case IndexedList.at idx model.contents of
                Just widget ->
                    let
                        ( newWidget, cmd ) =
                            Widget.update widgetMessage widget

                        newWidgets =
                            IndexedList.set idx newWidget model.contents
                    in
                        ( { model | contents = newWidgets }
                        , Cmd.map (EntryMsg idx) cmd
                        )

                _ ->
                    model ! []

        RefreshAll ->
            let
                updateResults =
                    model.contents
                        |> IndexedList.map (Widget.update Widget.Refresh)

                updatedModels =
                    updateResults |> IndexedList.map Tuple.first

                updateCmds =
                    updateResults
                        |> IndexedList.map Tuple.second
                        |> IndexedList.toList
                        |> List.map (\( idx, resCmd ) -> Cmd.map (EntryMsg idx) resCmd)
            in
                { model
                    | contents = updatedModels
                }
                    ! updateCmds

        AddNewCounter ->
            { model
                | contents =
                    model.contents
                        |> IndexedList.add (Widget.initCounter)
                        |> Tuple.second
            }
                ! [ Cmd.none ]



-- VIEW


view : Model -> Html Msg
view model =
    let
        entryView idx content =
            Widget.view content |> Html.map (EntryMsg idx)

        viewIndexedEntry ( index, content ) =
            entryView index content

        contentsHtml =
            model.contents |> IndexedList.toList |> List.map viewIndexedEntry
    in
        div []
            [ input [ value model.newTopic, onInput UpdateNewTopic ] []
            , button [ onClick AddNewTopic ] [ text "New Giphy" ]
            , button [ onClick AddNewCounter ] [ text "New Counter" ]
            , button [ onClick RefreshAll ] [ text "RefreshAll" ]
            , div [] contentsHtml
            ]



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
