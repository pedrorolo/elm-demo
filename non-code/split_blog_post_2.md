<!-- # Elm - Javascript reinvented (2 - more context)-->

This is second part of a series of articles about the Elm programming language. 

[On the first article(TODO)](TODO) I focus on the features that should make Elm an interesting alternative to Javascript.


[On this second article (TODO)](TODO) I rather focus on the historical perspective that makes Elm worth a look; elaborate on the languages two main features: functional purity and static-typing; and finally look at who is using Elm "in the real-world".

# The low-profile success of Elm's architecture

### Before the virtual dom

Before 2013, most frontend programming followed very imperative approaches. Frameworks like Backbone or Angular v1 inspired themselves on the MVC and made the mutation and observation of their view models the core of their display logic. Handling so much state and events was hard and uncomfortable for many web programmers - to whom the simple data-flow of their backend share-nothing web-frameworks made them sigh for the old days when Javascript and AJAX had not yet taken over their world.

### The one way data flow

[React](https://en.wikipedia.org/wiki/React_(JavaScript_library)) was the first project shaking this imperative house of cards. The most distinctive feature of this project is the virtual DOM, which enables programmers think of their web page as something that can be efficiently recalculated and rerendered without a big cost. Such efficiency is what allows their way to model the frontend: A web-page consists on a tree of components and each time the user interacts with them the whole page appearance is recalculated based on the new state stored within these components. You do not have to actively change and the DOM anymore. React takes care of that for you.

Despite that React made it easier to model web applications it did not prescribe a way to separate the business logic from the rendering logic. Facebook did propose an architecture called Flux to solve this problem, though many of the former implementations were rather more complex than needed and were taking time to gain traction.

### A functional architecture for React applications

It was in 2015 that [Redux](https://en.wikipedia.org/wiki/Redux_(JavaScript_library)) appeared and emerged as a simpler implementation of the Flux architecture. The main idea is that all the application state should be stored into a single state tree and that the changes to this state should be modeled as a reducer function. This reducer function shoul itself be the result of the composition of several reducer functions, each being responsible for part of the state of the application, modeling the logic to process each action and obtain a new versions of the state. 

Redux is a very simple and small library with a very small core (around a dozen of small functions). It was easy to grasp; pleasant to use and allows using cool development features like time-traveling debuggers and hot module reloading.

Despite its simplicity, its approach is not intuitive and feels alien to Javascript. Writing reducer functions that do not change the provided previous version of the state is not trivial and it is even recommended to use [a library](https://facebook.github.io/immutable-js/) to deal with this state in an immutable way. *It is thus with no big surprise that Redux author makes it very clear that such architecture has its origins amongst **Elm**: an obscure technology within the typed functional programming languages niche.*


# Static typing

So, if this architecture succeeded even when taken way from its comfort zone and blended with an alien imperative technology like Javascript, how nice can it be if kept away from the "hermeneutics" of dynamic typing and the unsafety of arbitrary state mutations? Is there more to take from this lode? Are there other secrets under this Elm? 

### The Javascript ecosystem seems to need static typing

When a software project starts, all the code is *beautiful, pink and shinny™* , and then there come the deadlines; bugs; hotfixes; scope changes; company merges; acquisitions; and a horde of over-allocated developers changing that project while somebody else is on vacation; sick or dead. After aging, every *name* caries the height of its story - which often nobody is there to tell.


On badly aged projects, variable names lie; function names lie; class names lie and type names lie. Though, when using dynamically typed languages - like Javascript - only names and tests provide us with information about the semantics of a program and about how it can be changed and improved. 

### In praise of static typing

Statically typed languages - on the other hand, check the semantics of the project and assure its correctness. The names might be wrong, though we know that certain operations correspond to certain types and the compiler automatically checks if our changes make sense. The information provided to the developers by such systems is worth a thousand of names; and those systems are easier to change and require much less testing because the type system avoids a big amount of errors that would otherwise have to be checked for.

### The downside of static-typing

The big problem with types is that - in most languages - they get in the developers' way when developing new code; debugging or experimenting how does the code behave. In most languages they add a lot of clunky noise to the clear speech that developers have with ~~the machine~~ other developers while coding. They are also forced to type much more - and while that might not be very bad while bringing the code into the editor - it is a complete catastrophe while experimenting in interactive environments like the command line or the debugger. The clunkiness of having the code full of type information creates plenty of inertia and implies a huge loss of agility. Choosing in between static and dynamic typing is thus a tradeoff in between *Usability and Maintainability*.

### The Javascript ecosystem is moving towards gradual typing

Is is thus with no surprise that we find several initiatives bringing static typing into the Javascript world. The most notorious ones are [Flow](https://flow.org/) (Facebook), [Typescript](https://en.wikipedia.org/wiki/TypeScript) (Microsoft), and [Dart](https://en.wikipedia.org/wiki/Dart_(programming_language)) (Google). While these are not changes to the Javascript language itself, they either consist of supersets of the language that add typing, or on very similar languages that directly compete with it. This is building plenty of pressure to move in the statically-typed direction when developing because the used tools and libraries are also moving that way.

However, there is a common trend amongst all these approaches: In order to avoid the static typed clunkiness they have chosen to make the types optional. Thus - depending on the context - the developers are free to add type information to their code, though when not doing it they are still able to work without type checking - and type safety. The results are better than without types, but still relatively limited: As the types are gradual, it is often impossible to check if the type signatures make sense, and the amount of verification depends heavily on the developer's discipline and on whether the used libraries have type information for the same type system.

### Elm has a different approach

Elm's type-system does not go in the gradual type direction. Elm's creator believes that gradual typing is a bad middle-ground and that there is another way to turn types into developers' best friends. 

His ideas come from the typed functional world, in which decades of development resulted in the use of type inference. Unlike with dynamic or gradual typing, the compiler checks the types of all the code. Though, unlike most static type systems, the type information is not required to be explicitly entered by the programmer, and is rather inferred by the compiler. The programmer may choose to later add type signatures to his functions, though these type signatures go separate from the function definitions. In this way, they do not become clunky to whoever later reads or edits the code.

It should be however noticed that while inspired in typed functional type systems, Elm does not adhere to their most complex features, only adopting what seems useful to frontend programmers and keeping a pragmatic approach. It rather like if Javascript was redesigned for its current usage. 


# Pure functional programming

Elm is a pure functional programming language. This means that all its expressions enjoy of a simple property called *Referential Transparency* from which stem several interesting consequences. We proceed by describing what is *Referential Transparency*  and then explore its consequences.

### Referential transparency

An expression or function is said to be referential transparent when its results do not depend on the context in which it is evaluated. By other words, when provided with a certain input, a referential transparent expression it will always evaluate to the same result. 


### Pure functions are easy to reason about and to test

If we think of the mathematical concept of *function* we may notice that it indeed enjoys this property. It is actually quite hard to imagine how could it be otherwise: The function that calculates the area of a square always evaluates to 4 square meters when provided with a 2-meters input, and will always behave as such. There is no way to write a function that will take the side of the square as single input and have it return different values at different moments in time. 

At least in the mathematics that most of us have been thought, mathematicians seem to have chosen such abstraction as a very important - if not the main - tool to model and reason about the world. I would say this was no accident: 

If a function always returns the same results independently from its context we can evaluate and test it without having anything in account except for its inputs and outputs. We know exactly what to provide and what to expect when thinking about; modeling and testing it. 

### No mutation

But referential transparency is not a reality in most programing languages. With the development of digital computers, most programming languages chose to model the world in a way akin to the way the machines internally work rather than to the one mathematicians used to model the world. There were attempts to follow the first path since the development of the first compilers, though, the computers were then not powerful enough to handle such computing model, and that machine-like way to model the world became an ingrained part of our computer-science culture.

That machine-like way to model the world (imperative programming) is mostly based on mutating state. In it, computations can be modeled by reading and changing the state of a memory until a final state is computed. When adding functions to this model, most languages chose to share this state amongst functions thus breaking referential transparency: Functions do not depend on their input. Their outcomes depend on the memory that they read and write, and they often do not return any value - solely serving the purpose of changing the values in the shared state.

In contrast, in purely functional programming, no state is shared amongst our functions, and if no state is shared, there ceases to be a reason to change the state of the values that are being used. The aim of our functions stops being changing values and starts being using them as inputs to calculate other values. In purely functional programming, every value is immutable.

Elm is fits on this language family, and thus all its values are immutable and all its functions are referentially transparent. 

### The runtime is in charge of side-effects

Pure functions alone are useless. We perform computations because we want to read input values from the outer world and because we want to change this outer world according to the values of our computations. The outer world is thus like a state that is altered by our program in the same sense as the memory of our imperative programs is. 

Some functional programming languages found mathematical ways to deal with this issue elegantly in pure-functional style and a general purpose way, but by using some concepts that have shown being quite hard to grasp.

Elm has a different approach. Its runtime and architecture hides this problem away from us. Our program ends up being a set of functions used to calculate what to display to the user given a certain sequence of user actions or other environmental inputs.

Basically, the Elm runtime and architecture take care of interacting with the world in this particular domain. And make it in a pleasant and simple way.



# Real-world Elm?

Elm's ecosystem is quite immature and is evolving slowly. However, the libraries quality is often very good and the compiler gives us plenty of guarantees about their stability. For now, Elm is appropriate for small dynamic webpage components or for simple webpages that do not require server-side rendering. 

There are some important features only expected to come with the following version (0.19) :

- Server-side rendering — sending HTML with the initial response
- Tree shaking — trimming out unused code (usually called dead-code elimination or DCE)
- Code splitting — cutting up code into smaller chunks for better caching
- Lazy loading — only sending the code chunks needed for a particular page

The main objective of this version will be providing an acceptable system to develop complex single page web applications.

### Real-world usage

Despite its immaturity, there are already some success stories in the industry:

- [NoRedInk](https://www.noredink.com/) is a company in the educational-software field.
- [Pivotal Tracker](https://www.pivotaltracker.com) is a famous company that provides tools for agile develpment. They have recently started using Elm and seem to be having **[a very nice experience](https://www.pivotaltracker.com/blog/Elm-pivotal-tracker/)** with it.
- [Prezi](https://prezi.com/) develop software to create interactive presentations in the browser.
- [CBANC Network](https://www.cbancnetwork.com/) - A network of banking professionals with tools for banks to collaborate as well as manage their business. All new frontend develpoment is being done in Elm
- [CircuitHub (GitHub)](https://circuithub.com/) - CircuitHub provide on demand electronics manufacturing with instant quotes.
- [Yary Labs](http://www.yarilabs.com/) - A portuguese company developing software using functional languages

- [Other companies](https://github.com/lpil/elm-companies): Adrima; Bendyworks; Futurice; Gizra; Test Double; Mimo; SMRxT;  TruQu; PinMeTo; Hearken; Day One; Spottt; Spottt; imby.bio; wonktonk; Beautiful Destinations; AS Tallink Grupp; CARFAX


# Related Languages

### Haskell

Haskell is by far the language that most influenced Elm. It is the current standard for lazy typed programming languages and is development by a committee of academics from the field. The main differences from Elm are:

- It is a general purpose programming language
- It provides a very polymorphic and complex type system.
- The evaluation is lazy (expressions are only evaluated when their values are needed)
- Usually is compiled to assembly
- Has runtime errors

### Purescript

[Purescript](http://www.purescript.org/) falls somewhere in between Haskell and Elm. Elm and Purescript are evolving together and influencing each other as they grow. It's main characteristics are: 

- General Purpose Programming Language
- Compiles to Javascript
- Like Haskell, sports a very polymorphic and complex type system
  - Worse error messages than Elm
- Uses Javascript's runtime rather than its own runtime
- Easy (and unsafe) interactions with Javascript code
- Like in Elm (and unlike in Haskell), it sports strict evaluation
- Has runtime errors

Due to its complex type system, I do not think it is feasible for most Javascript programmers to jump into Purescript. However, its easy (and unsafe) interaction with Javascript makes it an interesting option for people who might have gone through Elm's or Haskell's stepping stones.

There are three projects in this language's ecosystem that try to address the same problem as the Elm architecture:

* [Thermite](https://github.com/paf31/purescript-thermite) - A wrapper around React 
* [Halogen](https://github.com/slamdata/purescript-halogen) - A frontend framework taking advantage of the Purescript's complex type system
* [Pux](http://purescript-pux.org/) - When using Halogen they realized that the elaborate typing made if very hard for beginners, they then implemented Elm's architecture in Purescript, without deviating much from the simplicity of Elm's approach. The outcome was a framework is called Pux.

# Conclusion

Elm is an interesting frontend-specific programming environment that sports an immature software ecosystem. Due to the language design this young ecosystem ends up providing atypically strong safety guarantees. Many of these stem from the friendly compiler that garantees that there are no runtime errors.

It is very pleasant to work with and allows easy development and refactoring with unprecedented safety and joy. It was designed to fulfill the needs of a modern Javascript programmer and to be easy to get started with. 

For now it is not ready to develop complex single page web applications due to lacking server-side-rendering and some important optimizations. The next release (0.19) is expected to address these issues, and while it is not ready it might not be a good idea to use it for more than small applications or components. 

It is worth keeping some attention on this project, that seems to have the potential of becoming a very competitive tool.


