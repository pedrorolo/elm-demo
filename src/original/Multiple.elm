-- Read more about this program in the official Elm guide:
-- https://guide.elm-lang.org/architecture/effects/http.html


module Multiple exposing (..)

import Giphy
import IndexedList exposing (IndexedList)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html


main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { contents :
        IndexedList Giphy.Model
    , newTopic : String
    }


init : ( Model, Cmd Msg )
init =
    ( Model IndexedList.empty ""
    , Cmd.none
    )



-- UPDATE


type Msg
    = GiphyMessage Int Giphy.Msg
    | UpdateNewTopic String
    | AddNewTopic


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UpdateNewTopic s ->
            ( { model | newTopic = s }, Cmd.none )

        AddNewTopic ->
            let
                ( newModel, newCmd ) =
                    Giphy.init model.newTopic

                ( newIdx, newContents ) =
                    IndexedList.add newModel model.contents
            in
                { model | contents = newContents }
                    ! [ Cmd.map (GiphyMessage newIdx) newCmd ]

        GiphyMessage idx giphyMessage ->
            case IndexedList.at idx model.contents of
                Nothing ->
                    model ! []

                Just g ->
                    let
                        ( newGiphy, cmd ) =
                            Giphy.update giphyMessage g

                        newGiphies =
                            IndexedList.set idx newGiphy model.contents
                    in
                        ( { model | contents = newGiphies }
                        , Cmd.map (GiphyMessage idx) cmd
                        )



-- VIEW


view : Model -> Html Msg
view model =
    let
        viewIndexedEntry ( index, content ) =
            Html.map (GiphyMessage index) <| Giphy.view content

        contentsHtml =
            model.contents |> IndexedList.toList |> List.map viewIndexedEntry
    in
        div []
            [ input [ value model.newTopic, onInput UpdateNewTopic ] []
            , button [ onClick AddNewTopic ] [ text "New Giphy" ]
            , div [] contentsHtml
            ]



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
