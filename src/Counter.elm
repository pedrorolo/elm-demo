-- Read more about this program in the official Elm guide:
-- https://guide.elm-lang.org/architecture/user_input/buttons.html


module Counter exposing (..)

import Html exposing (beginnerProgram, div, button, text)
import Html.Events exposing (onClick)


main =
    beginnerProgram { model = init, view = view, update = update }


view model =
    div []
        [ button [ onClick Decrement ] [ text "-" ]
        , div [] [ text (toString model) ]
        , button [ onClick Increment ] [ text "+" ]
        ]


type Msg
    = Increment
    | Decrement
    | Reset


type alias Model =
    Int


init =
    0


update msg model =
    case msg of
        Increment ->
            model + 1

        Decrement ->
            model - 1

        Reset ->
            0


reset =
    update Reset
