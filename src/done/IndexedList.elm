module IndexedList exposing (IndexedList, add, set, at, toList, empty, map)

import Dict exposing (Dict)
import Tuple


type alias IndexedList a =
    { lastIdx : Int
    , dict : Dict Int a
    }


add : a -> IndexedList a -> ( Int, IndexedList a )
add a l =
    let
        lastIdx =
            l.lastIdx

        newIdx =
            lastIdx + 1

        newList =
            IndexedList newIdx <| Dict.insert newIdx a l.dict
    in
        ( newIdx, newList )


at : Int -> IndexedList a -> Maybe a
at i =
    .dict >> Dict.get i


set : Int -> a -> IndexedList a -> IndexedList a
set i new l =
    { l | dict = Dict.remove i l.dict |> Dict.insert i new }


map : (a -> b) -> IndexedList a -> IndexedList b
map f l =
    { l | dict = l.dict |> Dict.map (\_ v -> f v) }


empty : IndexedList a
empty =
    IndexedList 0 Dict.empty


toList : IndexedList a -> List ( Int, a )
toList =
    .dict >> Dict.toList
