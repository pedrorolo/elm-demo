It is nice to work during summer: The weather is nice, there's not so much traffic; people are not so rushy; the city is less noisy and sometimes there is some free time to have a look at tech _out-of-the-box_. 

I've been having a look at the Elm programming language and the Elm framework. They are a typed functional programming approach to the frontend that is influencing other popular technologies in a somewhat disruptive way. This article is a writeup on what I have been figuring out.

##### TL;DR;

- Extreme Safety - No runtime errors!
- Domain specific frontend framework & programming language designed to please Javascripters
- Friendly compiler error messages
- Like if [React](https://en.wikipedia.org/wiki/React_(JavaScript_library)); [Redux](https://en.wikipedia.org/wiki/Redux_(JavaScript_library)); [Immutable](https://facebook.github.io/immutable-js/) and [Flow](https://flow.org/) were bundled together into a convenient domain-specific language and framework
- [Pure functional language](https://en.wikipedia.org/wiki/Purely_functional_programming)
- Statically typed language with [type inference](https://en.wikipedia.org/wiki/Hindley%E2%80%93Milner_type_system) - writing type signatures is optional but there's always 100% type coverage
- Inspired by [Haskell](https://en.wikipedia.org/wiki/Haskell_(programming_language)) but much simpler than [Haskell](https://en.wikipedia.org/wiki/Haskell_(programming_language))


### The low-profile success of Elm's architecture

#### Before the virtual dom

Before 2013, most frontend programming followed very imperative approaches. Frameworks like Backbone or Angular v1 inspired themselves on the MVC and made the mutation and observation of their view models the core of their display logic. Handling so much state and events was hard and uncomfortable for many web programmers - to whom the simple data-flow of their backend share-nothing web-frameworks made them sigh for the old days when Javascript and AJAX had not yet taken over their world.

#### The one way data flow

[React](https://en.wikipedia.org/wiki/React_(JavaScript_library)) was the first project shaking this imperative house of cards. The most distinctive feature of this project is the virtual DOM, which enables programmers think of their web page as something that can be efficiently recalculated and rerendered without a big cost. Such efficiency is what allows their way to model the frontend: A web-page consists on a tree of components and each time the user interacts with them the whole page appearance is recalculated based on the new state stored within these components. You do not have to actively change and the DOM anymore. React takes care of that for you.

Despite that React made it easier to model web applications it did not prescribe a way to separate the business logic from the rendering logic. Facebook did propose an architecture called Flux to solve this problem, though many of the former implementations were rather more complex than needed and were taking time to gain traction.

#### A functional architecture for React applications

It was in 2015 that [Redux](https://en.wikipedia.org/wiki/Redux_(JavaScript_library)) appeared and emerged as a simpler implementation of the Flux architecture. The main idea is that all the application state should be stored into a single state tree and that the changes to this state should be modeled as a reducer function. This reducer function shoul itself be the result of the composition of several reducer functions, each being responsible for part of the state of the application, modeling the logic to process each action and obtain a new versions of the state. 

Redux is a very simple and small library with a very small core (around a dozen of small functions). It was easy to grasp; pleasant to use and allows using cool development features like time-traveling debuggers and hot module reloading.

Despite its simplicity, its approach is not intuitive and feels alien to Javascript. Writing reducer functions that do not change the provided previous version of the state is not trivial and it is even recommended to use [a library](https://facebook.github.io/immutable-js/) to deal with this state in an immutable way. *It is thus with no big surprise that Redux author makes it very clear that such architecture has its origins amongst **Elm**: an obscure technology within the typed functional programming languages niche.*

So, if this architecture succeeded even when taken way from its comfort zone and blended with an alien imperative technology like Javascript, how nice can it be if kept away from the "hermeneutics" of dynamic typing and the unsafety of arbitrary state mutations? Is there more to take from this lode? Are there other secrets under this Elm? 

### How does Elm reinvent front-end programming?

While so far I focused on the motivations behind Elm's core and on our motivations to have a look at it, on the following ones I will try to address what makes it interesting. I will thus have a look at each of these topics:

- Elm has a static typed system
- Elm is easy 
- **Elm is safe - There are no runtime errors!**
- Elm is fun
- Elm is a pure functional programming language


##### The Javascript ecosystem seems to need static typing

When a software project starts, all the code is *beautiful, pink and shinny™* , and then there come the deadlines; bugs; hotfixes; scope changes; company merges; acquisitions; and a horde of over-allocated developers changing that project while somebody else is on vacation; sick or dead. After aging, every *name* caries the height of its story - which often nobody is there to tell. 

On badly aged projects, variable names lie; function names lie; class names lie and type names lie. Though, when using dynamically typed languages  - like Javascript - only names and tests provide us with information about the semantics of a program and about how it can be changed and improved. 

###### In praise of static typing

Statically typed languages - on the other hand, check the semantics of the project and assure its correctness. The names might be wrong, though we know that certain operations correspond to certain types and the compiler automatically checks if our changes make sense. The information provided to the developers by such systems is worth a thousand of names; and those systems are easier to change and require much less testing because the type system avoids a big amount of errors that would otherwise have to be checked for.

###### The downside of static-typing

The big problem with types is that - in most languages - they get in the developers' way when developing new code; debugging or experimenting how does the code behave. In most languages they add a lot of clunky noise to the clear speech that developers have with ~~the machine~~ other developers while coding. They are also forced to type much more - and while that might not be very bad while bringing the code into the editor - it is a complete catastrophe while experimenting in interactive environments like the command line or the debugger. The clunkiness of having the code full of type information creates plenty of inertia and implies a huge loss of agility. Choosing in between static and dynamic typing is thus a tradeoff in between *Usability and Maintainability*.

###### The Javascript ecosystem is moving towards gradual typing

Is is thus with no surprise that we find several initiatives bringing static typing into the Javascript world. The most notorious ones are [Flow](https://flow.org/) (Facebook), [Typescript](https://en.wikipedia.org/wiki/TypeScript) (Microsoft), and [Dart](https://en.wikipedia.org/wiki/Dart_(programming_language)) (Google). While these are not changes to the Javascript language itself, they either consist of supersets of the language that add typing, or on very similar languages that directly compete with it. This is building plenty of pressure to move in the statically-typed direction when developing because the used tools and libraries are also moving that way.

However, there is though a common trend amongst all these approaches: In order to avoid the static typed clunkiness they have chosen to make the types optional. Thus - depending on the context - the developers are free to add type information to their code, though when not doing it they are still able to work without type checking - and type safety. The results are better than without types, but still relatively limited: As the types are gradual, it is often impossible to check if the type signatures make sense, and the amount of verification depends heavily on the developer's discipline and on whether the used libraries have type information for the same type system.

###### Elm has a different approach

Elm's type-system does not go in the gradual type direction. Elm's creator believes that gradual typing is a bad middle-ground and that there is another way to turn types into developers' best friends. 

His ideas come from the typed functional world, in which decades of development resulted in the use of type inference. Unlike with dynamic or gradual typing, the compiler checks the types of all the code. Though, unlike most static type systems, the type information is not required to be explicitly entered by the programmer, and is rather inferred by the compiler. The programmer may choose to later add type signatures to his functions, though these type signatures go separate from the function definitions. In this way, they do not become clunky to whoever later reads or edits the code.

It should be however noticed that while inspired in typed functional type systems, Elm does not adhere to their most complex features, only adopting what seems useful to frontend programmers and keeping a pragmatic approach. It rather like if Javascript was redesigned for its current usage. 


#### Elm is easy

When designing the language, Elm's author has chosen to put the user at the center of his design choices. If you take a look at his ["Let's be mainstream" presentation](http://www.elmbark.com/2016/03/16/mainstream-elm-user-focused-design) you will notice a strong focus on who is the user (The Javascript programmer) and what does he need, by employing the so-called *Usage-driven design*. This philosophy reduces the language features to the essential and makes it pretty easy to be learned.

##### Gradual Learning

This simplicity makes the language very prone to learning by doing. And the tooling around the language makes it really easy to get started. Go on, just get started and open [these interactive examples](http://elm-lang.org/examples)! Play with them! Change them right away in the browser! Debug them, explore their actions and the values of their states! This is not rocket science; no Haskell with its load of Monads and all the cryptic category theory that goes with it!

##### No Typeclasses 

That's right, this is no Haskell - Elm's main source of inspiration. While following the usage-driven design process many of Haskell's features were not needed and thus not added to the language. Amongst those was the infamous typeclasses system. 

That system would bring with it all the pretentious-sounding mathematical vocabulary typically heard amongst Haskell programmers and render Elm totally inappropriate for the "regular Joe".

But don't we need typeclasses? They must serve any purpose! Why does Haskell need them and Elm doesn't? - Well, the answer relies on the problem domain addressed by each of these languages: While Haskell Is a general-purpose programming language, Elm is not. On one hand, Elm is simply a frontend programming language and is able to to accomplish that task well without resorting to so much complexity. On the other, part of what such complexity aims to deal with is handled by and hidden in the Elm runtime - which takes care of handling side-effects and mutation in a domain-specific fashion.

##### Friendly error messages;

Elm's error messages are extremely friendly. On one hand, this comes from the static type system, that allows the compiler to clearly know and state the differences between the types of the values that are expected and the ones that are being provided to our functions. On the other, as there are no typeclasses the type information shown in error messages is very down-to-earth:

```
The 2nd argument to function `append` is causing a mismatch.

52| List.append listOfFloats [ "Banana" ]
                             ^^^^^^^^^^^^
Function `append` is expecting the 2nd argument to be:

    List Float

But it is:

    List String

Hint: I always figure out the type of arguments from left to right. If an
argument is acceptable when I check it, I assume it is "correct" in subsequent
checks. So the problem may actually be in how previous arguments interact with
the 2nd.
```

In addition to this atypical clarity, the language designer decided that error messages should be a priority, and thus, in addition to formally state what is wrong with the programs, the compiler also provides 'hints' about what might have been the programmer's intentions and provides suggestions of possible fixes, for instance:

```
Cannot find variable `map`

49| map 1 2
    ^^^
Maybe you want one of the following?

    max
    main
    min
    msg
```

#### Elm is safe

Elm's type system is very safe and there are two consequences of its design that are worth a notice: 

- There are no runtime errors
- There is no null values
- Control statements forced to deal with all possibilities
- Javascript interactions are "armoured"

##### There are no runtime errors

While other programming languages handle errors as a part of the language with very specific features - usually called Exceptions or Errors - Elm does not feature any specific primitives to trigger errors or to handle them. The possibility of failure is rather modeled trough the types of values that are returned and handled by functions. Some parametric types that may be used to convey information about failure are the types `Maybe`; `Result` and `Task`.

The `Maybe` type is the most simple of these. It is defined as:

```haskell
type Maybe a = Just a | Nothing
```

The `a` in this expression might be replaced by any specific type, e.g. a value of type `Maybe String`  may either assume a value like `Just "some specific string"` or `Nothing`. 

Thus a function returning a value of - for instance - `Maybe String` can return a value `Just "I am the returned string"` when succeding or return `Nothing` when failing. The programmer is later always forced to choose what to do in both cases and the language does not allow the program to compile if both possibilities are not handled.

##### No null value

It is through the previously shown approach that the language avoids having a null value. And thus get rid of all those usual "undefined is not a function" errors. If there might be a failure then the type will explicitly convey this information and the compiler will force this case to be handled.

##### All possible inputs must be handled on the control statements

As Elm is a pure functional language, all the point of evaluating something is to get a value from it. Thus, every control statement is forced provide a return value for each of its possible input values. This implies that there are no `if` statements with no `else` and that when using case statements when have to handle every possibility. 

For the sake of example, if there is an abstract data type `TypingDiscipline` that might assume as value `Static`; `Dynamic` or `Gradual`:

```haskell
-- akin to an enum
type TypingDiscipline = Static 
                      | Dynamic 
                      | Gradual
```
and if were handling some value of this type as the input of a case statement (a switch statement), but forget to handle one of the possibilities:

```haskell
-- assuming x is of type TypingDiscipline
case x of

  Static -> "I feel safe."

  Dynamic -> "Let's check an horoscope"
```
the compiler does not let us compile our program:
```
This `case` does not have branches for all possibilities. - You need to account for the following values:

    Gradual
```

Thus when changing our types to add more functionality we end up being notified by the compiler that there is some code needing to be added in order to deal with a new possibility.


#### Elm is fun

Elm's *short feedback loop* and *uncluttered syntax* make it a very pleasant environment to work with: I proceed looking a bit at how these to things contribute to the Elm's developer joy.

##### Short feedback loop

An interesting consequence of the previously described safety feaures is how it shortens the feedback loop. The programming activity usually consists on iterating through the following activities:

  - Thinking - thinking about the code to write
  - Typing - typing the code
  - Compiling - fix the code so that it compiles
  - Testing - test if the code does what it was supposed to

The last step is often where the developers spend most time, and this time is either spent writing automated tests or interacting with the program to confirm that it is correct. Most programmers would say this phase to be the least joyful one.

Elm's safety and lack of runtime exceptions transfers part of the *Testing* phase effort into the *Compiling* phase. When using Elm, the programmer will spend much longer on the compiling phase - interacting with the compiler until the program is considered correct - and much less time in the testing phase, when the only thing that may fail is whether the business requirements are met - and not whether the program crashes or not. 

When using Elm, the programmer will thus have feedback about his mistakes earlier in his workflow, not having to spend so long in the lengthy testing phase.


##### Uncluttered syntax

Elm's syntax is inspired by Haskell and minimalist. There are only two control statements; and very few reserved words.

The center of this syntax is the function. If the most used syntactic feature of any structured programming language is the function invocation, then why do most Algol-style languages resort to so many special characters for function invocation and definition?

```Javascript
function myFunction(arg1, arg2){ 
  functionBody();
}

myfunction(3, 2);
```

  In order to enter the above definition and invocation we had to enter 8 special characters for the function definition and 4 on the function invocation. Depending on your keyboard's language, the amount of pressed modifier-keys; annoyance and premature repetitive-strain-injury will vary.

  Elm cleans this up. If the function is such an important thing it should be easily typed and edited:

```Haskell
myFunction arg1 arg2 = 
  functionBody

myFunction 3 2
```

As you see, this function definition did not go with any type signature. Type signatures are optional and automatically inferred by the compiler, so that we can swiftly experiment without having to explicitly enter them. Nonetheless, they are sometimes useful: Sometimes it is handy to have the type signature as guidance while writing a less obvious function and most often they are a very useful form of documentation. Thus, in Elm type signatures are (optionally) written in a separate line (some editors will generate the type signature automatically when we ask for it!):

```Haskell
factorial : Integer -> Integer
factorial n = 
  if n == 0 then 
    1 
  else 
    n * factorial (n - 1)
```

Other feature that makes the language rather fun to use is that every function is curried and may thus be applied to a subset of its arguments, returning a so called partially-applied function that awaits only the missing arguments. Thus, the following code :

```Haskell
-- function with three args
add3 a b c = a + b + c 

{- function with two args created by partially applying add3 
   only to the first argument -}
add1to2args = add3 1

{- applying the previously created function to the remaining 
   arguments -}
add1to2args 2 3  --> 6 
```

corresponds to the following Javscript code:

```Javascript
//curried fuction with three arguments
add3 = (a) => (b) => (c) => {a + b + c}

/* function with two arguments created by applying 
   add3 only to the first argument */
add1to2args = add3(1)

/* applying the previously created function to the 
   remaining arguments */
add1to2args(2)(3) //=> 6
```

The main advantage of this feature is that code using higher-order-functions becomes very clean:

```Haskell
isBiggerThan x y = x < y

List.filter (isBiggerThan 5) [1, 2, 3, 4]

power x =  x * x

List.map power [1, 2, 3, 4] --> [1, 4, 9, 16] 
```

Though if we chain these operations we start to get a sea of nested parens typical from a Lisp:

```Haskell
List.filter (isBiggerThan 1) 
  (List.map power
    (List.filter (isBiggerThan 5) 
      [1, 4, 9, 16])) --> [81, 256]
```

To avoid this problem, the language provides the pipe operators (`<|` and `|>`). They allow chaining function application with a syntax akin to threading an argument through a pipeline of transformations:

```Haskell 
[1, 4, 9, 16]
  |> List.filter (isBiggerThan 5)
  |> List.map power
  |> List.filter (isBiggerThan 1) --> [81,256]
```

In a similar fashion; you can also use the composition (`<<`) or reverse-composition (`>>`) operators to express the same without having to explicitly refer the argument that is being threaded:


```Haskell
functionWithPipe : List Int -> List Int
functionWithPipe arg =  
  arg
    |> List.filter (isBiggerThan 5)
    |> List.map power
    |> List.filter (isBiggerThan 1)

functionWithComposition : List Int -> List Int
functionWithComposition =  
  List.filter (isBiggerThan 5)
    >> List.map power
    >> List.filter (isBiggerThan 1)
```

This conveniently succinct syntax, together with the nice error messages end up making Elm very fit for interactive experimentation, both by using the REPL and by using the compiler.


##### Nice tooling

When you start an Elm project you immediately get plenty of functionality out-of-the-box:

* A rendering framework based on virtual Dom (as *React*)
* A state container (as *Redux*)
* The immutability that in JS is usually accomplished by using *Immutable* is part of the Elm language definition
* Type checking - similar to - but stronger - than the one you may get by using *Flow*
* An interactive development environment similar to what is provided by *Webpack*, called *Elm Reactor*
* A package manager called elm-package

Aditionally it is worth elaborating on some other tools:

###### Time-traveling debugger

This is one of the things Redux got from implementing the Elm architecture. When developing and trying a program we are able to inspect the actions performed on that interface and to go back and forth in time in order to analyze the state of the program at each moment of its execution.

In Redux this functionality is rather fragile due to the imperative nature of Javascript, and in order to cope with it one is required to use `Immutable` or other library to avoid directly mutating the state of the application. In Elm, this problem simply does not exist, because there's no such thing as mutation at the language level. The language design itself protects us from this kind of problems and from the need of using additional libraries.


###### Online editors

Additionally, you may fiddle with the language directly on the web-browser - without having to install anything. There are two sites providing live-editing functionality:
  
- [The Online Editor](http://elm-lang.org/try), which comes with several small example programs and that only allows you to use the core packages and to edit single-file programs.
- [Ellie](https://ellie-app.com/new), good for experimenting with more than one file and allowing the use of additional packages that do not belong in the core of the language.

###### Automatically enforced semantic version

When publishing a new package version, the Elm tooling will check whether the functions that are exported by our modules:

- changed their signatures; 
- only exported new functions; 
- or did not change the signatures of the exported functions. 

According to each of these possibilities the change will be reflected on the automatically calculated package's version number, that will let it clear whether the changes are major changes; minor changes or bugfixes; and whether it is safe to upgrade the version of a library that is being used.

#### Elm is a pure functional programming language

Elm is a pure functional programming language. This means that all its expressions enjoy of a simple property called *Referential Transparency* from which stem several interesting consequences. We proceed by describing what is *Referential Transparency*  and then explore its consequences.

##### Referential transparency

An expression or function is said to be referential transparent when its results do not depend on the context in which it is evaluated. By other words, when provided with a certain input, a referential transparent expression it will always evaluate to the same result. 


##### Pure functions are easy to reason about

If we think of the mathematical concept of *function* we may notice that it indeed enjoys of this property. It is actually quite hard to imagine how could it be otherwise: The function that calculates the area of a square always evaluates to 4 square meters when provided with a 2-meters input, and will always behave as such. There is no way to write a function that will take the side of the square as single input and have it return different values at different moments in time. 

At least in the mathematics that most of us have been thought, mathematicians seem to have chosen such abstraction as a very important - if not the main - tool to model and reason about the world. I would say this was no accident: 

If a function always returns the same results independently from its context we can evaluate and test it without having anything in account except for its inputs and outputs. We know exactly what to provide and what to expect when thinking about; modeling and testing it. 

##### No mutation

But referential transparency is not a reality in most programing languages. With the development of digital computers, most programming languages chose to model the world in a way akin to the way the machines internally work rather than to the one mathematicians used to model the world. There were attempts to follow the first path since the development of the first compilers, though, the computers were then not powerful enough to handle such computing model, and that machine-like way to model the world became an ingrained part of our computer-science culture.

That machine-like way to model the world (imperative programming) is mostly based on mutating state. In it, computations can be modeled by reading and changing the state of a memory until a final state is computed. When adding functions to this model, most languages chose to share this state amongst functions thus breaking referential transparency: Functions do not depend on their input. Their outcomes depend on the memory that they read and write, and they often do not return any value - solely serving the purpose of changing the values in the shared state.

In contrast, in purely functional programming, no state is shared amongst our functions, and if no state is shared, there ceases to be a reason to change the state of the values that are being used. The aim of our functions stops being changing values and starts being using them as inputs to calculate other values. In purely functional programming, every value is immutable.

Elm is fits on this language family, and thus all its values are immutable and all its functions are referentially transparent. 

##### The runtime is in charge of side-effects

Pure functions alone are useless. We perform computations because we want to read input values from the outer world and because we want to change this outer world according to the values of our computations. The outer world is thus like a state that is altered by our program in the same sense as the memory of our imperative programs is. 

Some functional programming languages found mathematical ways to deal with this issue elegantly in pure-functional style and a general purpose way, but by using some concepts that have shown being quite hard to grasp.

Elm has a different approach. Its runtime and architecture hides this problem away from us. Our program ends up being a set of functions used to calculate what to display to the user given a certain sequence of user actions or other environmental inputs.

Basically, the Elm runtime and architecture take care of interacting with the world in this particular domain. And make it in a pleasant and simple way.


### Real-world Elm?

Elm's ecosystem is quite immature and is evolving slowly. However, the libraries quality is often very good and the compiler gives us plenty of guarantees about their stability. For now, Elm is appropriate for small dynamic webpage components or for simple webpages that do not require server-side rendering. 

There are some important features only expected to come with the following version (0.19) :

- Server-side rendering — sending HTML with the initial response
- Tree shaking — trimming out unused code (usually called dead-code elimination or DCE)
- Code splitting — cutting up code into smaller chunks for better caching
- Lazy loading — only sending the code chunks needed for a particular page

The main objective of this version will be providing an acceptable system to develop complex single page web applications.

###### Real-world usage

Despite its immaturity, there are already some success stories in the industry:

- [NoRedInk](https://www.noredink.com/) is a company in the educational-software field.
- [Pivotal Tracker](https://www.pivotaltracker.com) is a famous company that provides tools for agile develpment. They have recently started using Elm and seem to be having **[a very nice experience](https://www.pivotaltracker.com/blog/Elm-pivotal-tracker/)** with it.
- [Prezi](https://prezi.com/) develop software to create interactive presentations in the browser.
- [CBANC Network](https://www.cbancnetwork.com/) - A network of banking professionals with tools for banks to collaborate as well as manage their business. All new frontend develpoment is being done in Elm
- [CircuitHub (GitHub)](https://circuithub.com/) - CircuitHub provide on demand electronics manufacturing with instant quotes.
- [Yary Labs](http://www.yarilabs.com/) - A portuguese company developing software using functional languages

- [Other companies](https://github.com/lpil/elm-companies): Adrima; Bendyworks; Futurice; Gizra; Test Double; Mimo; SMRxT;  TruQu; PinMeTo; Hearken; Day One; Spottt; Spottt; imby.bio; wonktonk; Beautiful Destinations; AS Tallink Grupp; CARFAX


### Related Languages

##### Haskell

Haskell is by far the language that most influenced Elm. It is the current standard for lazy typed programming languages and is development by a committee of academics from the field. The main differences from Elm are:

- It is a general purpose programming language
- It provides a very polymorphic and complex type system.
- The evaluation is lazy (expressions are only evaluated when their values are needed)
- Usually is compiled to assembly
- Has runtime errors

##### Purescript

[Purescript](http://www.purescript.org/) falls somewhere in between Haskell and Elm. Elm and Purescript are evolving together and influencing each other as they grow. It's main characteristics are: 

- General Purpose Programming Language
- Compiles to Javascript
- Like Haskell, sports a very polymorphic and complex type system
  - Worse error messages than Elm
- Uses Javascript's runtime rather than its own runtime
- Easy (and unsafe) interactions with Javascript code
- Like in Elm (and unlike in Haskell), it sports strict evaluation
- Has runtime errors

Due to its complex type system, I do not think it is feasible for most Javascript programmers to jump into Purescript. However, its easy (and unsafe) interaction with Javascript makes it an interesting option for people who might have gone through Elm's or Haskell's stepping stones.

There are three projects in this language's ecosystem that try to address the same problem as the Elm architecture:

* [Thermite](https://github.com/paf31/purescript-thermite) - A wrapper around React 
* [Halogen](https://github.com/slamdata/purescript-halogen) - A frontend framework taking advantage of the Purescript's complex type system
* [Pux](http://purescript-pux.org/) - When using Halogen they realized that the elaborate typing made if very hard for beginners, they then implemented Elm's architecture in Purescript, without deviating much from the simplicity of Elm's approach. The outcome was a framework is called Pux.

### Conclusion

Elm is an interesting frontend-specific programming environment that sports an immature software ecosystem. Due to the language design this young ecosystem ends up providing atypically strong safety guarantees. Many of these stem from the friendly compiler that garantees that there are no runtime errors.

It is very pleasant to work with and allows easy development and refactoring with unprecedented safety and joy. It was designed to fulfill the needs of a modern Javascript programmer and to be easy to get started with. 

For now it is not ready to develop complex single page web applications due to lacking server-side-rendering and some important optimizations. The next release (0.19) is expected to address these issues, and while it is not ready it might not be a good idea to use it for more than small applications or components. 

It is worth keeping some attention on this project, that seems to have the potential of becoming a very competitive tool.


