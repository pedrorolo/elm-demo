module IndexedList exposing (IndexedList, add, set, at, toList, empty)

import Dict exposing (Dict)


type alias IndexedList a =
    Dict Int a


add : a -> IndexedList a -> ( Int, IndexedList a )
add a l =
    let
        lastIdx =
            l
                |> Dict.keys
                |> List.maximum
                |> Maybe.withDefault 0

        newIdx =
            lastIdx + 1

        newList =
            Dict.insert newIdx a l
    in
        ( newIdx, newList )


at : Int -> IndexedList a -> Maybe a
at =
    Dict.get


set : Int -> a -> IndexedList a -> IndexedList a
set i new =
    Dict.remove i >> Dict.insert i new


empty : IndexedList a
empty =
    Dict.empty


toList : IndexedList a -> List ( Int, a )
toList =
    Dict.toList


map : (a -> b) -> IndexedList a -> IndexedList b
map mapper =
    Dict.map (\idx val -> mapper val)
