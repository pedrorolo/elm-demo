-- Read more about this program in the official Elm guide:
-- https://guide.elm-lang.org/architecture/effects/http.html


module Giphy exposing (main, Msg(Search), Model, init, update, view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as Decode


main =
    Html.program
        { init = init "cats"
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { topic : String
    , gifUrl : String
    }


waitingImgUrl =
    "https://media2.giphy.com/media/l3nWhI38IWDofyDrW/giphy.gif"


errorImgUrl =
    "https://media2.giphy.com/media/g8GfH3i5F0hby/giphy.gif"


init : String -> ( Model, Cmd Msg )
init topic =
    ( Model topic waitingImgUrl
    , getRandomGif topic
    )



-- UPDATE


type Msg
    = Search
    | NewGif (Result Http.Error String)
    | ChangeTopic String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Search ->
            ( { model | gifUrl = waitingImgUrl }, getRandomGif model.topic )

        NewGif (Ok newUrl) ->
            ( Model model.topic newUrl, Cmd.none )

        NewGif (Err _) ->
            ( { model | gifUrl = errorImgUrl }, Cmd.none )

        ChangeTopic s ->
            ( { model | topic = s }, Cmd.none )



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ input [ value model.topic, onInput ChangeTopic ] []
        , button [ onClick Search ] [ text "Search" ]
        , br [] []
        , img
            [ src model.gifUrl, style [ ( "max-height", "300px" ) ] ]
            []
        ]



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- HTTP


getRandomGif : String -> Cmd Msg
getRandomGif topic =
    let
        url =
            "https://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC&tag=" ++ topic
    in
        Http.send NewGif (Http.get url decodeGifUrl)


decodeGifUrl : Decode.Decoder String
decodeGifUrl =
    Decode.at [ "data", "image_url" ] Decode.string
