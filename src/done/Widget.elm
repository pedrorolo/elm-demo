module Widget exposing (Model, Msg, update, view, initGiphy, initCounter, reset)

import Giphy exposing (Model, Msg, update, view, init)
import Counter exposing (Msg, update, view, init)
import Html exposing (Html)


type Model
    = GiphyWidget Giphy.Model
    | CounterWidget Counter.Model


type Msg
    = GiphyMsg Giphy.Msg
    | CounterMsg Counter.Msg


mapResult : (m -> Model) -> (msg -> Msg) -> ( m, Cmd msg ) -> ( Model, Cmd Msg )
mapResult modelMapper msgMapper =
    Tuple.mapFirst modelMapper >> Tuple.mapSecond (Cmd.map msgMapper)


mapGiphyResult =
    mapResult GiphyWidget GiphyMsg


mapCounterResult =
    mapResult CounterWidget CounterMsg


initGiphy =
    Giphy.init >> mapGiphyResult


initCounter =
    ( CounterWidget Counter.init, Cmd.none )


view model =
    let
        viewMap msgConstructor view =
            view >> Html.map msgConstructor
    in
        case model of
            CounterWidget counter ->
                viewMap CounterMsg Counter.view counter

            GiphyWidget giphy ->
                viewMap GiphyMsg Giphy.view giphy


update msg model =
    case ( model, msg ) of
        ( CounterWidget counter, CounterMsg msg ) ->
            ( Counter.update msg counter, Cmd.none ) |> mapCounterResult

        ( GiphyWidget giphy, GiphyMsg msg ) ->
            Giphy.update msg giphy |> mapGiphyResult

        ( _, _ ) ->
            ( model, Cmd.none )


reset model =
    case model of
        CounterWidget _ ->
            update (CounterMsg Counter.Reset) model

        GiphyWidget _ ->
            update (GiphyMsg Giphy.MorePlease) model
