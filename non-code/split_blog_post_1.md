<!-- # Elm - Javascript reinvented (1 - overview) -->

It is nice to work during summer: The weather is nice, there's not so much traffic; people are not so rushy; the city is less noisy and sometimes there is some free time to have a look at tech _out-of-the-box_. 

I've been having a look at the Elm programming language and the Elm framework. They are a typed functional programming approach to the frontend that is influencing other popular technologies in a somewhat disruptive way. These two articles are writeup on what I have been figuring out about it.


On this first article I focus on the features that should make Elm an interesting alternative to Javascript.


[On the second article (TODO)](TODO) I rather focus on the historical perspective that makes Elm worth a look; elaborate on the languages two main features: functional purity and static-typing; and finally look at who is using Elm in the real world.

# What is Elm?

Elm is a framework and typed functional programming language aimed at front-end development. It inspires itself in a programming language called Haskell to bring more mantainability to Javascript, though keeping itself as simple and close to Javascript as possible. The architecture of the framework is similar to Redux, that acctually was heavily inspired in Elm.


# Elm is easy

When designing the language, Elm's author has chosen to put the user at the center of his design choices. If you take a look at his ["Let's be mainstream" presentation](http://www.elmbark.com/2016/03/16/mainstream-elm-user-focused-design) you will notice a strong focus on who is the user (The Javascript programmer) and what does he need, by employing the so-called *Usage-driven design*. This philosophy reduces the language features to the essential and makes it pretty easy to be learned.


### Friendly error messages

Elm's error messages are extremely friendly. On one hand, this comes from the static type system, that allows the compiler to clearly know and state the differences between the types of the values that are expected and the ones that are being provided to our functions. On the other, as there are no typeclasses the type information shown in error messages is very down-to-earth:

```
The 2nd argument to function `append` is causing a mismatch.

52| List.append listOfFloats [ "Banana" ]
                             ^^^^^^^^^^^^
Function `append` is expecting the 2nd argument to be:

    List Float

But it is:

    List String

Hint: I always figure out the type of arguments from left to right. If an
argument is acceptable when I check it, I assume it is "correct" in subsequent
checks. So the problem may actually be in how previous arguments interact with
the 2nd.
```

In addition to this atypical clarity, the language designer decided that error messages should be a priority, and thus, in addition to formally state what is wrong with the programs, the compiler also provides 'hints' about what might have been the programmer's intentions and provides suggestions of possible fixes, for instance:

```
Cannot find variable `map`

49| map 1 2
    ^^^
Maybe you want one of the following?

    max
    main
    min
    msg
```

### No Typeclasses 

That's right, this is no Haskell - Elm's main source of inspiration. While following the usage-driven design process many of Haskell's features were not needed and thus not added to the language. Amongst those was the infamous typeclasses system. 

That system would bring with it all the pretentious-sounding mathematical vocabulary typically heard amongst Haskell programmers and render Elm totally inappropriate for the "regular Joe".

But don't we need typeclasses? They must serve any purpose! Why does Haskell need them and Elm doesn't? - Well, the answer relies on the problem domain addressed by each of these languages: While Haskell Is a general-purpose programming language, Elm is not. On one hand, Elm is simply a frontend programming language and is able to to accomplish that task well without resorting to so much complexity. On the other, part of what such complexity aims to deal with is handled by and hidden in the Elm runtime - which takes care of handling side-effects and mutation in a domain-specific fashion.


# Safety

Elm's type system is very safe and there are two consequences of its design that are worth a notice: 

- There are no runtime errors
- There is no null values
- Control statements forced to deal with all possibilities

### There are no runtime errors

While other programming languages handle errors as a part of the language with very specific features - usually called Exceptions or Errors - Elm does not feature any specific primitives to trigger errors or to handle them. The possibility of failure is rather modeled trough the types of values that are returned and handled by functions. Some parametric types may be used to convey information about failure, such as the types `Maybe`; `Result` and `Task`.

The `Maybe` type is the most simple of these. It is defined as:

```haskell
type Maybe a = Just a | Nothing
```

The `a` in this expression might be replaced by any specific type, e.g. a value of type `Maybe String`  may either assume a value like `Just "some specific string"` or `Nothing`. 

Thus a function returning a value of - for instance - `Maybe String` can return a value `Just "I am the returned string"` when succeding or return `Nothing` when failing. The programmer is later always forced to choose what to do in both cases and the language does not allow the program to compile if both possibilities are not handled.

### No null value

It is through the previously shown approach that the language avoids having a null value. And thus get rid of all those usual `undefined is not a function` errors. If there might be a failure then the type will explicitly convey this information and the compiler will force this case to be handled by the programmer.

### All possible inputs must be handled on the control statements

As Elm is a pure functional language, all the point of evaluating something is to get a value from it. Thus, every control statement is forced provide a return value for each of its possible input values. This implies that there are no `if` statements with no `else` and that when using case statements when have to handle every possibility. 

For the sake of example, if there is an abstract data type `TypingDiscipline` that might assume as value `Static`; `Dynamic` or `Gradual`:

```haskell
-- akin to an enum
type TypingDiscipline = Static 
                      | Dynamic 
                      | Gradual
```
and if were handling some value of this type as the input of a case statement (a switch statement), but forget to handle one of the possibilities:

```haskell
-- assuming x is of type TypingDiscipline
case x of

  Static -> "I feel safe."

  Dynamic -> "Let's check an horoscope"
```
the compiler does not let us compile our program:
```
This `case` does not have branches for all possibilities. - You need to account for the following values:

    Gradual
```

Thus when changing our types to add more functionality we end up being notified by the compiler that there is some code needing to be added in order to deal with a new possibility.


# Elm is fun

Elm's *short feedback loop* and *uncluttered syntax* make it a very pleasant environment to work with: I proceed looking a bit at how these to things contribute to the Elm's developer joy.

### Short feedback loop

An interesting consequence of the previously described safety feaures is how it shortens the feedback loop. The programming activity usually consists on iterating through the following activities:

  - Thinking - thinking about the code to write
  - Typing - typing the code
  - Compiling - fix the code so that it compiles
  - Testing - test if the code does what it was supposed to

The last step is often where the developers spend most time, and this time is either spent writing automated tests or interacting with the program to confirm that it is correct. Most programmers would say this phase to be the least joyful one.

Elm's safety and lack of runtime exceptions transfers part of the *Testing* phase effort into the *Compiling* phase. When using Elm, the programmer will spend much longer on the compiling phase - interacting with the compiler until the program is considered correct - and much less time in the testing phase, when the only thing that may fail is whether the business requirements are met - and not whether the program crashes or not. 

When using Elm, the programmer will thus have feedback about his mistakes earlier in his workflow, not having to spend so long in the lengthy testing phase.


### Uncluttered syntax

Elm's syntax is inspired by Haskell and minimalist. There are only two control statements; and very few reserved words.

The center of this syntax is the function. If the most used syntactic feature of any structured programming language is the function invocation, then why do most Algol-style languages resort to so many special characters for function invocation and definition?

```Javascript
function myFunction(arg1, arg2){ 
  functionBody();
}

myfunction(3, 2);
```

  In order to enter the above definition and invocation we had to enter 8 special characters for the function definition and 4 on the function invocation. Depending on your keyboard's language, the amount of pressed modifier-keys; annoyance and premature repetitive-strain-injury will vary.

  Elm cleans this up. If the function is such an important thing it should be easily typed and edited:

```Haskell
myFunction arg1 arg2 = 
  functionBody

myFunction 3 2
```

As you see, this function definition did not go with any type signature. Type signatures are optional and automatically inferred by the compiler, so that we can swiftly experiment without having to explicitly enter them. Nonetheless, they are sometimes useful: Sometimes it is handy to have the type signature as guidance while writing a less obvious function and most often they are a very useful form of documentation. Thus, in Elm type signatures are (optionally) written in a separate line (some editors will generate the type signature automatically when we ask for it!):

```Haskell
factorial : Integer -> Integer
factorial n = 
  if n == 0 then 
    1 
  else 
    n * factorial (n - 1)
```

Other feature that makes the language rather fun to use is that every function is curried and may thus be applied to a subset of its arguments, returning a so called partially-applied function that awaits only the missing arguments. Thus, the following code :

```Haskell
-- function with three args
add3 a b c = a + b + c 

{- function with two args created by partially applying add3 
   only to the first argument -}
add1to2args = add3 1

{- applying the previously created function to the remaining 
   arguments -}
add1to2args 2 3  --> 6 
```

corresponds to the following Javscript code:

```Javascript
//curried fuction with three arguments
add3 = (a) => (b) => (c) => {a + b + c}

/* function with two arguments created by applying 
   add3 only to the first argument */
add1to2args = add3(1)

/* applying the previously created function to the 
   remaining arguments */
add1to2args(2)(3) //=> 6
```

The main advantage of this feature is that code using higher-order-functions becomes very clean:

```Haskell
isBiggerThan x y = x < y

List.filter (isBiggerThan 5) [1, 2, 3, 4]

power x =  x * x

List.map power [1, 2, 3, 4] --> [1, 4, 9, 16] 
```

Though if we chain these operations we start to get a sea of nested parens typical from a Lisp:

```Haskell
List.filter (isBiggerThan 1) 
  (List.map power
    (List.filter (isBiggerThan 5) 
      [1, 4, 9, 16])) --> [81, 256]
```

To avoid this problem, the language provides the pipe operators (`<|` and `|>`). They allow chaining function application with a syntax akin to threading an argument through a pipeline of transformations:

```Haskell 
[1, 4, 9, 16]
  |> List.filter (isBiggerThan 5)
  |> List.map power
  |> List.filter (isBiggerThan 1) --> [81,256]
```

In a similar fashion; you can also use the composition (`<<`) or reverse-composition (`>>`) operators to express the same without having to explicitly refer the argument that is being threaded:


```Haskell
functionWithPipe : List Int -> List Int
functionWithPipe arg =  
  arg
    |> List.filter (isBiggerThan 5)
    |> List.map power
    |> List.filter (isBiggerThan 1)

functionWithComposition : List Int -> List Int
functionWithComposition =  
  List.filter (isBiggerThan 5)
    >> List.map power
    >> List.filter (isBiggerThan 1)
```

This conveniently succinct syntax, together with the nice error messages end up making Elm very fit for interactive experimentation, both by using the REPL and by using the compiler.


### Nice tooling

When you start an Elm project you immediately get plenty of functionality out-of-the-box:

* A rendering framework based on virtual Dom (as *React*)
* A state container (as *Redux*)
* The immutability that in JS is usually accomplished by using *Immutable* is part of the Elm language definition
* Type checking - similar to - but stronger - than the one you may get by using *Flow*
* An interactive development environment similar to what is provided by *Webpack*, called *Elm Reactor*
* A package manager called elm-package

Additionally it is worth elaborating on some other tools:

#### Time-traveling debugger

This is one of the things Redux got from implementing the Elm architecture. When developing and trying a program we are able to inspect the actions performed on that interface and to go back and forth in time in order to analyze the state of the program at each moment of its execution.

In Redux this functionality is rather fragile due to the imperative nature of Javascript, and in order to cope with it one is required to use `Immutable` or other library to avoid directly mutating the state of the application. In Elm, this problem simply does not exist, because there's no such thing as mutation at the language level. The language design itself protects us from this kind of problems and from the need of using additional libraries.


#### Online editors

Additionally, you may fiddle with the language directly on the web-browser - without having to install anything. There are two sites providing live-editing functionality:
  
- [The Online Editor](http://elm-lang.org/try), which comes with several small example programs and that only allows you to use the core packages and to edit single-file programs.
- [Ellie](https://ellie-app.com/new), good for experimenting with more than one file and allowing the use of additional packages that do not belong in the core of the language.

#### Automatically enforced semantic version

When publishing a new package version, the Elm tooling will check whether the functions that are exported by our modules:

- changed their signatures; 
- only exported new functions; 
- or did not change the signatures of the exported functions. 

According to each of these possibilities the change will be reflected on the automatically calculated package's version number, that will let it clear whether the changes are major changes; minor changes or bugfixes; and whether it is safe to upgrade the version of a library that is being used.

# To be continued

[On the next article (TODO)](TODO) I detail about why static typing and functional purity might make sense given the current Javascript's ecosystem tendencies and will describe these in more detail. 


